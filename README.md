##WORD COUNT | MULTI-THREADING | JAVA | DESIGN PATTERNS

##Project description

Code Language 			: Java

Patterns Implemented	: Visitor Pattern 

•	Created an application where one visitor populates a tree with the words in file. 

•	Another visitor counts the number of words, number of unique words, and number of characters in the tree and stores it in output file. 