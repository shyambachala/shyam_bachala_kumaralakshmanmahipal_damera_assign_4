package wordTree.store;

import java.io.File;

import wordTree.util.FileDisplayInterface;
import wordTree.util.FileProcessor;
import wordTree.util.MyLogger;
import wordTree.util.MyLogger.DebugLevel;
import wordTree.util.StdoutDisplayInterface;

/**
 * Results Class
 */
public class Results implements StdoutDisplayInterface, FileDisplayInterface
{

	/** The count words. */
	private int count_words;
	
	/** The distinct words. */
	private int distinct_words;
	
	/** The number of chars. */
	private int num_chars;
	
	/**
	 * results class constructor.
	 */
	public Results()
	{
		MyLogger.writeMessage("Constructor in Results Called", DebugLevel.CONSTRUCTOR);
		count_words = 0;
		distinct_words = 0;
		num_chars = 0;
	}
	  
	
	/* writeToFile method
	 * write the results obtained to the output file
	 * @param outf the output file name
	 */
	public void writeSchedulesToFile(File outf)
	{
		FileProcessor buff_writer = new FileProcessor(outf, false);
		buff_writer.writeOutput("The total number of words: " + getCount_words() + "\n" + 
								"The total number of characters: " + getNum_chars() + "\n" + 
								"The total number of distinct words: " + getDistinct_words() + "\n");
		
	}
	  
	
	/* writeToStdout
	 * Print the output obtained to the Stdout
	 */
	public void writeToScreen()
	{
		System.out.println( "The total number of words: " + getCount_words() + "\n" + 
							"The total number of characters: " + getNum_chars() + "\n" + 
							"The total number of distinct words: " + getDistinct_words() + "\n");
	}
	
	
	/* toString method
	 * Debugger method used to check the output
	 */
	public String toString()
	{
		return "The total number of words: " + getCount_words() + "\n" + 
				"The total number of characters: " + getNum_chars() + "\n" + 
				"The total number of distinct words: " + getDistinct_words() + "\n";
	}

	/**
	 * Gets the count words.
	 *
	 * @return the count words
	 */
	public int getCount_words() {
		return count_words;
	}

	/**
	 * Sets the count words.
	 *
	 * @param count_words the new count words
	 */
	public void setCount_words(int count_words) {
		this.count_words = count_words;
	}

	/**
	 * Gets the distinct words.
	 *
	 * @return the distinct words
	 */
	public int getDistinct_words() {
		return distinct_words;
	}

	/**
	 * Sets the distinct words.
	 *
	 * @param distinct_words the new distinct words
	 */
	public void setDistinct_words(int distinct_words) {
		this.distinct_words = distinct_words;
	}

	/**
	 * Gets the number of characters.
	 *
	 * @return the number of characters
	 */
	public int getNum_chars() {
		return num_chars;
	}

	/**
	 * Sets the number of characters.
	 *
	 * @param num_chars the number of characters
	 */
	public void setNum_chars(int num_chars) {
		this.num_chars = num_chars;
	}

}
