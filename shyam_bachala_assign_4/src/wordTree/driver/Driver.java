package wordTree.driver;

import java.io.File;

import wordTree.store.Results;
import wordTree.threadMgmt.CreateWorkers;
import wordTree.threadMgmt.generateResults;
import wordTree.util.FileProcessor;
import wordTree.util.MyLogger;

/**
 * Driver Class
 */
public class Driver {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{
		if(args.length != 5)
		{
			System.out.println("INVALID NUMBER OF INPUT PARAMETERS: " + args.length + "\nGIVE INPUT AS BELOW\n" 
		                         + "ant -buildfile src/build.xml run -Darg0=/home/input_file/input.txt -Darg1=/home/input_file/output.txt -Darg2=NUM_THREADS "
		                         + "-Darg3=WORDS_TO_BE_DELETED -Darg4=DEBUG_VALUE");
			System.exit(4);
		}
		
		
//INPUT FILE
		File infile = new File(args[0]);

//OUTPUT FILE
		File outfile= new File(args[1]);
		int num_threads = Integer.parseInt(args[2]);
		String delete_words = args[3];
		int debug_value = Integer.parseInt(args[4]);

		MyLogger.setDebugValue(debug_value);
		
//CHECKING WHETHER THE NUMBER OF THREADS COMMAND LINE ARGUMENT IS BETWEEN 1 AND 3
		if((num_threads > 3) || (num_threads < 0))
		{
			System.err.println("Invalid Number of threads");
			System.exit(3);
		}
		
//CHECKING WHETHER THE NUMBER OF DELETE WORDS IS EQUAL TO NUMBER OF THREADS
		if(num_threads != (delete_words.split(" ")).length)
		{
			System.err.println("Invalid Number of delete words");
			System.exit(4);
		}
		
//CHECKING WHETHER THE DEBUG VALUE IS VALID
		if((debug_value > 4) || (debug_value < 0))
		{
			System.err.println("Invalid Debug value");
			System.exit(5);
		}
		
//CHECKING INPUT.TXT IS EMPTY
		if(infile.length() == 0)
		{
			System.out.println("ERROR: input.txt IS EMPTY");
			System.exit(2);
		}
		
//CREATING FILEPROCESSOR FOR READING
		FileProcessor fread = new FileProcessor(infile, true);
		FileProcessor fwrite = new FileProcessor(outfile, false);
		
//RESULTS CLASS INSTANCE
		Results temp = new Results();
		
//CREATEWORKERS CLASS INSTANCE
		CreateWorkers cw = new CreateWorkers(fread, fwrite, temp, num_threads, delete_words, debug_value);
		
//START POPULATEWORKERS FOR THREADS TO READ THE INPUT FILE
		cw.startPopulateWorkers(num_threads);
		
//GENERATERESULTS CLASS INSTANCE WHICH IS USED FOR 
//COMPUTING THE RESULTS AND WRITING TO THE RESULTS CLASS INSTANCE
		generateResults rR = new generateResults(cw.getRoot_node(), temp);
		
//START DELETEWORKSERS FOR THREADS TO DECREASE THE COUNT OF THE DELETE WORDS IN THE TREE
		cw.startDeleteWorkers(num_threads);
		
//RESULTS COMPUTED WRITTEN TO THE RESULTS CLASS INSTANCE
		rR.tree_traverse1();
		rR.tree_traverse2();
		rR.updateResults();
		temp.writeSchedulesToFile(outfile);
		
//PRINT TO SCREEN RESULTS FOR DEBUGGING WHETHER THE RESULTS WRITTEN TO FILE
//AND THAT COMPUTED ARE SAME OR NOT
		if(debug_value == 1)
		{
			temp.writeToScreen();
		}
	}

}
