package wordTree.util;

/**
 * Interface StdoutDisplayInterface.
 */
public interface StdoutDisplayInterface 
{
	
	/**
	 * Write to screen.
	 */
	void writeToScreen();
}
