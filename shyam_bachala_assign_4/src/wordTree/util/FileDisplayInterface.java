package wordTree.util;

import java.io.File;

/**
 * Interface FileDisplayInterface.
 */
public interface FileDisplayInterface 
{
	
	/**
	 * Write schedules to file.
	 *
	 * @param s the file name
	 */
	void writeSchedulesToFile(File s);
}
