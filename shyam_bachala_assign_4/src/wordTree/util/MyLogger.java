package wordTree.util;


/**
 * MyLogger class.
 */
public class MyLogger 
{
	/*

	DEBUG_VALUE=4 [Print to stdout everytime a constructor is called]
    DEBUG_VALUE=3 [Print to stdout a thread run method is called]
    DEBUG_VALUE=2 [Print the word for which the count is decreased]
    DEBUG_VALUE=1 [Print the computed statistics of the tree after delete 
    				threads completes work]
    DEBUG_VALUE=0 [No output should be printed from the application to stdout.
                   It is ok to write to the output file though"]
    */

  /**
	 * The Enum DebugLevel.
	 */
	public static enum DebugLevel 
	{
		
		/** The release. */
		RELEASE, 
		
		/** The in run. */
		IN_RUN, 
		
		/** The debug delete tree. */
		DEBUG_DELETE_TREE, 
		
		/** The in results. */
		IN_RESULTS, 
		
		/** The constructor. */
		CONSTRUCTOR
	};

  /** The debug level. */
	private static DebugLevel debugLevel;


  /**
   * Sets the debug value.
   *
   * @param levelIn the new debug value
   */
	public static void setDebugValue (int levelIn) 
	{
		switch (levelIn) 
		{
			case 4: debugLevel = DebugLevel.CONSTRUCTOR; break;
			case 3: debugLevel = DebugLevel.IN_RUN; break;
			case 2: debugLevel = DebugLevel.DEBUG_DELETE_TREE; break;
			case 1: debugLevel = DebugLevel.IN_RESULTS; break;
			case 0: debugLevel = DebugLevel.RELEASE; break;
		}
	}

  /**
   * Sets the debug value.
   *
   * @param levelIn the new debug value
   */
	public static void setDebugValue (DebugLevel levelIn) 
	{
		debugLevel = levelIn;
	}

  /**
   * Write message.
   *
   * @param message the message
   * @param levelIn the level in
   */
	public static void writeMessage (String  message, DebugLevel levelIn ) 
	{
		if (levelIn == debugLevel)
			System.out.println(message);
	}


  /* 
   * toString() used for debugging
   */
  	public String toString() 
  	{
  		return "Debug Level is " + debugLevel;
  	}
}
