package wordTree.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import wordTree.util.MyLogger.DebugLevel;


/**
 * FileProcessor Class.
 */
public class FileProcessor 
{
	
	/** The file reader. */
	FileReader file_reader = null;
    
    /** The buff reader. */
    BufferedReader buff_reader = null;
    
    /** The fname. */
    File fname = null;

    /** The file writer. */
    FileWriter file_writer = null;
    
    /** The buff writer. */
    BufferedWriter buff_writer = null;
    
    /** The oname. */
    File oname = null;

    /**
     * file processor class constructor.
     *
     * @param infilename the infilename
     * @param file_check the file check
     */
    public FileProcessor(File infilename, boolean file_check)
    {
    	if(file_check)
    	{
    		fname = infilename;
   
    		try
    		{
    			buff_reader = new BufferedReader(file_reader = new FileReader(infilename));
    		}
    		catch (FileNotFoundException ex) 
    		{
    			System.out.println("\nException : File not Found");
    			ex.printStackTrace();
    			System.exit(4);
    		}
    		finally {}
    	}
    	else
    	{
   			oname = infilename;            
            try
            {
            	buff_writer = new BufferedWriter(file_writer = new FileWriter(oname));
            }
            catch (IOException ex) 
    	    {
            	System.out.println("\nIO Exception : Couldn't access the Write file\n");
    			ex.printStackTrace();
            }
            finally {}
    	}
    	MyLogger.writeMessage("FileProcessor Class Constructor has been called", DebugLevel.CONSTRUCTOR);
    }

    /**
     * Removes the file streams.
     */
    public  void remove_dev()
    {
      try 
      {
    	  if (file_reader != null)
    		  file_reader.close();
    	  if (buff_reader != null)
    		  buff_reader.close();
      } 
      catch (IOException ex) 
      {
    	  System.err.format("\nIO Exception : Closing the file reader streams\n");
    	  ex.printStackTrace();
      }
      finally
      {}
    }
	
    /**
     * Write output to the file by the file writer.
     *
     * @param s the file name
     */
    public void writeOutput(String s)
	{
		try 
    	{
			buff_writer.write(s);
		} 
    	catch (IOException e)
    	{
    		System.out.println("\nIO Exception: Write to file \n");
			e.printStackTrace();
		}
    	finally
    	{
			try 
			{
				if (buff_writer != null)
					buff_writer.close();
				if (buff_writer != null)
					file_writer.close();
			} 
			catch (IOException e) 
			{
				System.out.println("\nIO Exception: Closing the file writer streams\n");
				e.printStackTrace();
			}
    	}
    	
	}
    
    /**
     * Read line by line form the file.
     *
     * @return the string
     */
	public synchronized String readLine()
    {
        String line = "";
        try
        {
            line = buff_reader.readLine();
        }
        catch (Exception e)
        {
        	System.err.format("\nException :Trying to read");
            e.printStackTrace();
        }
        finally
        {}
        return line;
    }

}
