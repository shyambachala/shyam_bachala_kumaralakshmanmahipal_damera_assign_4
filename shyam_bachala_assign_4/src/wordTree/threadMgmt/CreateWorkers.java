package wordTree.threadMgmt;


import wordTree.store.Results;
import wordTree.util.FileProcessor;
import wordTree.util.MyLogger;
import wordTree.util.MyLogger.DebugLevel;

/**
 *  CreateWorkers Class
 */
public class CreateWorkers 
{
	
	/** file reader. */
	FileProcessor freader = null;
	
	/** file writer. */
	FileProcessor fwriter = null;
	
	/** The results class instance. */
	Results r = null;
	
	/** The number of threads. */
	int num_threads = 0;
	
	/** The delete words. */
	String delete_words ="";
	
	/** The debug value. */
	int debug_value = 0;
	
	/** The root node. */
	treeBuild root_node = null;
	
	/**
	 * CreateWorkers class constructor.
	 *
	 * @param pr the filereader
	 * @param pw the filewriter
	 * @param t the results class instance
	 * @param nt the number of threads
	 * @param dw the delete words
	 * @param dv the debug value
	 */
	public CreateWorkers(FileProcessor pr, FileProcessor pw, Results t, int nt, String dw, int dv)
	{
		MyLogger.writeMessage("Constructor in CreateWorkers Called", DebugLevel.CONSTRUCTOR);
		freader = pr;
		fwriter = pw;
		r = t;
		num_threads = nt;
		delete_words = dw;
		debug_value = dv;
		root_node = new treeBuild();
		root_node.setRoot_node(root_node);
		
	}
	
	/**
	 * Prints the Tree.
	 */
	public void printT()
	{
		root_node.tree_print();
		System.out.println(root_node.node_concat_print);
	}
	
	/**
	 * Start populate workers.
	 *
	 * @param num the number of threads
	 */
	public void startPopulateWorkers(int num)
	{
		Thread[] t = new Thread[num];
		for(int i = 0; i < t.length; i++)
		{
			t[i] = new Thread(new PopulateThread(this.freader, this.root_node));
			t[i].start();
		}
		for(int i = 0; i < t.length; i++)
		{
			try
			{
				t[i].join();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
			finally
			{}
		}
	}
	
	/**
	 * Start delete workers.
	 *
	 * @param num the number of threads
	 */
	public void startDeleteWorkers(int num)
	{
		Thread[] t = new Thread[num];
		String[] del_w = delete_words.split(" ");
		for(int i = 0; i < t.length; i++)
		{
			t[i] = new Thread(new DeleteThread(this.root_node, del_w[i]));
			t[i].start();
		}
		for(int i = 0; i < t.length; i++)
		{
			try
			{
				t[i].join();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
			finally
			{}
		}
	}
	
	/**
	 * Gets the root node.
	 *
	 * @return the root node of the tree
	 */
	public treeBuild getRoot_node()
	{
		return root_node;
	}
}

