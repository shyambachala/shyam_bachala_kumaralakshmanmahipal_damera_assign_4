package wordTree.threadMgmt;

import java.util.ArrayList;

import wordTree.util.FileProcessor;
import wordTree.util.MyLogger;
import wordTree.util.MyLogger.DebugLevel;

/**
 * PopulateThread class.
 */
public class PopulateThread implements Runnable 
{
	
	/** The file reader. */
	FileProcessor freader = null;
	
	/** The tree root node. */
	treeBuild root_node = null;
	
	/**
	 * populate thread class constructor.
	 *
	 * @param fread the file reader
	 * @param r the tree node
	 */
	public PopulateThread(FileProcessor fread, treeBuild r)
	{
		MyLogger.writeMessage("Constructor in PopulateThread called", DebugLevel.CONSTRUCTOR);
		freader = fread;
		root_node = r;
	}


	@Override
	public void run() 
	{
		MyLogger.writeMessage("PopulateThread run() method called", DebugLevel.IN_RUN);
		String inputLine = "";
		ArrayList<String> tree_word_list = new ArrayList<String>();
		boolean check_condition = false;
		while((inputLine = freader.readLine()) != null)
	    {
	      	if(!(inputLine.trim().length() > 0))
	      	{
	      		continue;
	      	}
	      		
	       	String[] node_inputs = inputLine.split(" ");

//	       	CHECK FOR COURSE NAME
	       	if(node_inputs.length == 0)
	       	{
	       		continue;
	       	}

	       	
//	       	CHECKER FOR ROOT IS CREATED OR NOT 
	       	for (int i = 0; i < node_inputs.length; i++)
	       	{
       			try
       			{
       				check_condition = tree_word_list.contains(node_inputs[i]);
	       				
//	       			CHECK IF NODE FOR THE b_NUMBER IS ALREADY CREATED OR NOT
       				if(!check_condition)
       				{
       					tree_word_list.add(node_inputs[i]);
       					boolean f_bool;
       					f_bool = root_node.root_insert(root_node, node_inputs[i]);
       					if(!f_bool)
       					{
       						treeBuild n_node = new treeBuild(node_inputs[i]);
       						root_node.node_insert(n_node, node_inputs[i]);
       					}
       				}
       				
//	       			IF NODE ALREADY PRESENT ADD THE COURSE TO THE CORRESPONDING NODE
       				if(check_condition) 
       				{
       					root_node.node_increase_count(node_inputs[i]);
       				}
       			}
       			catch(Exception e)
       			{
       				System.out.println("\nException : NumberFormatException\n");
       				e.printStackTrace();
       			}
       			finally
       			{}
	       	}
	    }

	}

}
