package wordTree.threadMgmt;

import java.util.HashSet;

import wordTree.store.Results;
import wordTree.util.MyLogger;
import wordTree.util.MyLogger.DebugLevel;

/**
 * The Class generateResults.
 */
public class generateResults 
{
	
	/** The tree. */
	treeBuild new_tree = null; 
	
	/** The results class. */
	Results new_r = null;
	
	/** The count words. */
	int count_words = 0;
	
	/** The distinct words. */
	int distinct_words = 0;
	
	/** The number of characters. */
	int num_chars = 0;
	
	/** The tree word list. */
	HashSet<String> tree_word_list = new HashSet<String>();
	
	/**
	 * results class constructor.
	 *
	 * @param t the tree node
	 * @param r the results class object
	 */
	public generateResults(treeBuild t, Results r)
	{
		MyLogger.writeMessage("Constructor in generateResults Called", DebugLevel.CONSTRUCTOR);
		new_tree = t;
		new_r = r;	
	}
	
	/**
	 * Tree traverse 1.
	 */
	public void tree_traverse1()
	{
		tree_traverse_sub1(new_tree.getRoot_node());
	}
	
	/**
	 * Tree traverse sub 1.
	 * Compute the number of words and number of 
	 * characters in the file using the tree.
	 * @param t_node the tree node
	 */
	private void tree_traverse_sub1(treeBuild t_node) 
	{
		if(t_node != null)
		{
			tree_traverse_sub1(t_node.node_zero);
			count_words += t_node.word_count;
			num_chars += t_node.word_count*t_node.word_name.length();
			if(t_node.word_count > 0)
			{
				tree_word_list.add(t_node.word_name);
			}
			tree_traverse_sub1(t_node.node_one);
		}
	}
	

	/**
	 * Tree traverse 2.
	 * Compute the number of distinct words in the file using the tree.
	 */
	public void tree_traverse2()
	{
		distinct_words = tree_word_list.size();
	}
	
	/**
	 * Update results class object with the computed results.
	 */
	public void updateResults()
	{
		new_r.setCount_words(count_words);
		new_r.setDistinct_words(distinct_words);
		new_r.setNum_chars(num_chars);
		MyLogger.writeMessage("Number of words : " + count_words + "\n" + "Number of characters: " + num_chars + "\n" + 
				"Number of distinct words: " + distinct_words + "\n", DebugLevel.IN_RESULTS);
	}
}
