package wordTree.threadMgmt;

import wordTree.util.MyLogger;
import wordTree.util.MyLogger.DebugLevel;

/**
 * treeBuild class.
 */
public class treeBuild 
{
	
	/** The root node. */
	private treeBuild root_node = null;
	
	/** The node zero. */
	treeBuild node_zero = null;
	
	/** The node one. */
	treeBuild node_one = null;
	
	/** The root node check. */
	boolean root_node_check;
	
	/** The word count. */
	int word_count = 0;
	
	/** The node concatenate print. */
	String node_concat_print = "";
	
	/** The word name. */
	String word_name = "";
	
	/**
	 * tree build class constructor.
	 *
	 * @param w_name the word name
	 */
	public treeBuild(String w_name) 
	{
		MyLogger.writeMessage("Parameterised Constructor in treeBuild called", DebugLevel.CONSTRUCTOR);
		word_name = w_name;
		word_count = 1;
	}
	
	/**
	 * tree build class empty constructor.
	 */
	public treeBuild() 
	{
		root_node_check = false;
		MyLogger.writeMessage("Constructor in treeBuild called", DebugLevel.CONSTRUCTOR);
	}
	
	/**
	 * Root insert.
	 *
	 * @param n_node the tree node
	 * @param w_name the word name
	 * @return true, if successful
	 */
	public synchronized boolean root_insert(treeBuild n_node, String w_name)
	{
		if(n_node.root_node_check == false)
		{
			n_node.word_name = w_name;
			n_node.word_count = 1;
			n_node.root_node_check = true;
			return true;
		}
		return false;
	}
		
	/**
	 * Node insert.
	 *
	 * @param n_node the tree node
	 * @param w_name the word name
	 */
	public synchronized void node_insert(treeBuild n_node, String w_name)
	{
			treeBuild ins_node_ptr = node_insert_traversal(w_name);	
			if(ins_node_ptr != null)
			{
				if(w_name.compareTo(ins_node_ptr.word_name) < 0)
					ins_node_ptr.node_zero = n_node;
				else
					ins_node_ptr.node_one = n_node;
			}
	}
	
	/**
	 * Node insert traversal.
	 *
	 * @param w_name the word name
	 * @return the tree build node
	 */
	private treeBuild node_insert_traversal(String w_name)
	{
		treeBuild t_node = getRoot_node();
		treeBuild p_node = null;
		while(t_node != null)
		{
			if(w_name.compareTo(t_node.word_name) < 0)
			{
				p_node = t_node;
				t_node = t_node.node_zero;
			}
			else
			{
				p_node = t_node;
				t_node = t_node.node_one;
			}
		}
		return p_node;
	}
	
	/**
	 * Node increase word count.
	 *
	 * @param w_name the word name
	 */
	public synchronized void node_increase_count(String w_name)
	{
		treeBuild ins_node_ptr = node_increase_count_traversal(w_name);	
		if(ins_node_ptr != null)
		{
			ins_node_ptr.word_count += 1;
		}
	}
	
	/**
	 * Node increase word count traversal.
	 *
	 * @param w_name the word name
	 * @return the tree build node
	 */
	private treeBuild node_increase_count_traversal(String w_name)
	{
		treeBuild t_node = getRoot_node();
		treeBuild p_node = null;
		while(t_node != null)
		{
			if(w_name.compareTo(t_node.word_name) < 0)
			{
				p_node = t_node;
				t_node = t_node.node_zero;
			}
			else if(w_name.compareTo(t_node.word_name) == 0)
			{
				return t_node;
			}
			else
			{
				p_node = t_node;
				t_node = t_node.node_one;
			}
		}
		return p_node;
	}
	
	/**
	 * Node decrease count of word.
	 *
	 * @param w_name the word name
	 */
	public synchronized void node_decrease_count(String w_name)
	{
		treeBuild ins_node_ptr = node_decrease_count_traversal(w_name);
		if(ins_node_ptr != null)
		{
			if(ins_node_ptr.word_count > 0)
				ins_node_ptr.word_count -= 1;
		}
	}

	/**
	 * Node decrease count traversal.
	 *
	 * @param s_node_word the decrease count of delete word
	 * @return the tree build node
	 */
	private treeBuild node_decrease_count_traversal(String s_node_word)
	{
		treeBuild t_node = getRoot_node();
		treeBuild p_node = null;
		while(t_node != null)
		{
			if(s_node_word.compareTo(t_node.word_name) < 0)
			{
				p_node = t_node;
				t_node = t_node.node_zero;
			}
			else if(s_node_word == t_node.word_name)
			{
				return t_node;
			}
			else
			{
				p_node = t_node;
				t_node = t_node.node_one;
			}
		}
		return p_node;
	}
	
	/**
	 * Tree print.
	 */
	public void tree_print()
	{
		tree_print_sub(getRoot_node());
	}
	
	/**
	 * Tree print sub.
	 *
	 * @param t_node the tree node
	 */
	private void tree_print_sub(treeBuild t_node) 
	{
		if(t_node != null)
		{
			tree_print_sub(t_node.node_zero);
			treeBuild r_n = getRoot_node();
			r_n.node_concat_print = r_n.node_concat_print + "W_name: " + t_node.word_name + " --> Count: " + t_node.word_count + "\n";
			tree_print_sub(t_node.node_one);
		}
	}
	
	
	/**
	 * Gets the root node.
	 *
	 * @return the root node
	 */
	public treeBuild getRoot_node() 
	{
		return root_node;
	}

	/**
	 * Sets the root node.
	 *
	 * @param r_node the new root node
	 */
	public void setRoot_node(treeBuild r_node) 
	{
		this.root_node = r_node;
	}

}
