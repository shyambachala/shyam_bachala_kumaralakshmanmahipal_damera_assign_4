package wordTree.threadMgmt;

import wordTree.util.MyLogger;
import wordTree.util.MyLogger.DebugLevel;

/**
 * DeleteThread Class.
 */
public class DeleteThread implements Runnable
{
	
	/** The root node. */
	treeBuild root_node = null;
	
	/** The delete word. */
	String del_word = "";
	
	/**
	 * delete thread class constructor.
	 *
	 * @param r the root_node
	 * @param w the delete word
	 */
	public DeleteThread(treeBuild r, String w)
	{
		MyLogger.writeMessage("Constructor in DeleteThread called", DebugLevel.CONSTRUCTOR);
		root_node = r;
		del_word = w;
	}

	
	@Override
	public void run() 
	{
		MyLogger.writeMessage("DeleteThread run() method called", DebugLevel.IN_RUN);
		tree_traverse();
	}

	/**
	 * Tree traverse.
	 * Function used for traversing through the tree 
	 * decreasing the count of the delete word of present.
	 */
	private void tree_traverse()
	{
		tree_traverse_sub(root_node);
	}
	
	/**
	 * Tree traverse sub.
	 *
	 * @param t_node the tree_node
	 */
	private void tree_traverse_sub(treeBuild t_node) 
	{
		if(t_node != null)
		{
			tree_traverse_sub(t_node.node_zero);
			if(t_node.word_name.equals(del_word) && (t_node.word_count > 0))
			{
				MyLogger.writeMessage("Instance of " + t_node.word_name + " Deleted", DebugLevel.DEBUG_DELETE_TREE);
				t_node.word_count -= 1;
			}
			tree_traverse_sub(t_node.node_one);
		}
	}
	
}
