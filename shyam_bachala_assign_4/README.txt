## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile:

ant -buildfile src/build.xml all

Description: Compiles your code and generates .class files inside the BUILD
folder.

-----------------------------------------------------------------------

## To run by code:

ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt -Darg2=DEBUG_VALUE

Description:

Testing the input.txt file and delete.txt has been done by placing it in the [shyam_bachala_assign_3/airportSecurityState]
as mentioned in the design requirements.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
## To create tarball for submission

ant -buildfile src/build.xml tarzip

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

